import PmachineTable from './PmachineTable';
import PmachineCreateForm from './PmachineCreateForm';
import PmachineUpdateForm from './PmachineUpdateForm';
import PmachineFilter from './PmachineFilter';
import PmachineBatchButtonGroup from './PmachineBatchButtonGroup';

export default {
  PmachineTable,
  PmachineCreateForm,
  PmachineUpdateForm,
  PmachineFilter,
  PmachineBatchButtonGroup,
};
