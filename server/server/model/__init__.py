from server.model.base import BaseModel
from server.model.product import Product
from server.model.milestone import Milestone
from server.model.mirroring import IMirroring, QMirroring, Repo
from server.model.pmachine import Pmachine
from server.model.vmachine import Vmachine, Vdisk, Vnic
from server.model.testcase import Suite, Case
from server.model.template import Template
from server.model.job import Job, Analyzed, Logs
